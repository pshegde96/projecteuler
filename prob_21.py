from pylab import *

data = zeros(100001);
marked = zeros(100001);

def sum_finder(number):
	marked = zeros(number);
	sums = 1;
	factor = 2;
	while factor < sqrt(number):	
		if marked[factor]==0:
			if number%factor == 0:
				sums += factor;
				sums += number/factor;
			else:
				i = factor;
				while i < number:
					marked[i]=1;
					i+=factor;
		factor+=1;
	if number%int(sqrt(number))==0:
		sums+= int(sqrt(number));
	return sums;
			 	

for number in range(1,10001):
	data[number] = sum_finder(number);
	
for number in range(1,10001):
	if data[number]<10001:
		if data[data[number]]==number and data[number] != number:
			marked[number]=1;
			marked[data[number]] = 1;
	else:
		temp = sum_finder(data[number]);
		if temp == number:
			marked[number] = 1;

print sum(where(marked==1)[0]);
