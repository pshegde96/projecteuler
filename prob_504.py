from pylab import *

def funct(x1,y1,x2,y2,ax,ay,bx,by):
	z = ((y1-y2)*(ax-x1)+(x2-x1)*(ay-y1))*((y1-y2)*(bx-x1)+(x2-x1)*(by-y1));
	return z;

total_count=0;
for a in range(1,5):
	for b in range(1,5):
		for c in range(1,5):
			for d in range(1,5):
				count = 0;
				for y in range(1,b):
					#first quadrant:
					xp = a-1;
					while xp>0:
						z =funct(0,b,a,0,xp,y,0,0);
						if z>0:
							break;
						xp = xp-1;
					#2nd quadrant:
					xn = -c+1;
					while xn<0:
						z=funct(0,b,-c,0,xn,y,0,0);
						if z>0:
							break;
						xn = xn+1;
					count = count + (xp-xn+1);

				for y in arange(-1,-d,-1):
					#fourth quadrant:
					xp = a-1;
					while xp>0:
						z=funct(0,-d,a,0,xp,y,0,0)
						if z>0:
							break;
						xp = xp-1;
					#third quadrant:
					xn = -c+1;
					while xn<0:
						z=funct(0,-d,-c,0,xn,y,0,0)
						if z>0:
							break;
						xn = xn+1;
					count = count + (xp-xn+1);
				count = count + (a+c-1);
				if sqrt(count) in range(1,100000):
					total_count+=1;
				
print total_count;					
