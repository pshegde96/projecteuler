from pylab import *

size = 21;
lattice = zeros((size,size));

lattice[0,:]=1;  #only 1 way to reach points in first row
lattice[:,0]=1;  #only 1 way to reach points in first column

#No of ways to reach the other points in sum of the way to reach it from the previous points.
for i in range(1,size):
	for j in range(1,size):
		lattice[i,j]=lattice[i-1,j]+lattice[i,j-1];

print lattice[size-1,size-1];
