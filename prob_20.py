from pylab import *

product = zeros(200,dtype=int);
count = 1;
product[0] = 1;

while count<=100:
	carry = 0;
	for i in range(0,200):
		temp=product.copy();
		temp[i] = (product[i]*count+carry)%10;
		carry = (product[i]*count+carry)/10;
		product = temp;	
	count+=1;
	
print sum(product);
	
