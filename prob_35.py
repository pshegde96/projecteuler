from pylab import *

marked = [0] * 1000000
value = 3
primes = [0]*1000000;
primes[2] = 1;

def get_next_rotated(number):
	last_digit = number%10;
	count = 0;
	for j in str(number):
		count+=1;
	rotated_number = last_digit*(10**(count-1)) + int(number/10);
	return rotated_number;

while value < 1000000:
    if marked[value] == 0:
        primes[value]=1;
        i = value
        while i < 1000000:
            marked[i] = 1
            i += value
    value += 2
results = [0]*1000000;
circular_prime_count = 0;
for i in range(2,1000000):
	if primes[i]==1:
#		print "Hello"
#		print i;
		num = get_next_rotated(i);
#		print num;
		flag = 1;
		while num!=i:
			if primes[num]==0:
				flag = 0;
				break;
			num = get_next_rotated(num);
		if flag==1:
			results[i] = 1;
			circular_prime_count += 1;

print circular_prime_count;
