from pylab import *

grid = loadtxt("data_prob81.txt");
grid_size = size(grid[0,:]);
path_sum = grid;

i = grid_size-2;
while i>=0:
	path_sum[i,grid_size-1] += path_sum[i+1,grid_size-1];
	i-=1;

i = grid_size-2;
while i>=0:
	path_sum[grid_size-1,i] += path_sum[grid_size-1,i+1];
	i-=1;


i = grid_size-2;

while i>=0:
	j = grid_size-2;
	while j>=0:
		path_sum[i,j]=grid[i,j]+min(path_sum[i+1,j],path_sum[i,j+1]);
		j-=1;
	i-=1;
print path_sum[0,0];

