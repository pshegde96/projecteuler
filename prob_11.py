from pylab import *

data = loadtxt("data_prob11.txt");

def max_prod_finder(row,col):
	global data;
	max_prod = data[0,0]*data[row,col]*data[2*row,2*col]*data[3*row,3*col];
	for i in range(0,20-3*row):
		for j in range(0,20-3*col):
			prod = data[i,j]*data[i+row,j+col]*data[i+2*row,j+2*col]*data[i+3*row,j+3*col];
			max_prod = max(max_prod,prod);
	return max_prod;

def diag_max_prod():
	global data;
	max_prod = data[19,0]*data[18,1]*data[17,2]*data[16,3];
	for i in range(3,20):
		for j in range(0,17):
			prod = data[i,j]*data[i-1,j+1]*data[i-2,j+2]*data[i-3,j+3];
			max_prod = max(max_prod,prod);
	return max_prod;

			

max1 = max_prod_finder(0,1);
max2 = max_prod_finder(1,0);
max3 = max_prod_finder(1,1);
max4 = diag_max_prod();
max_product = max(max1,max2,max3,max4);
print max_product;
