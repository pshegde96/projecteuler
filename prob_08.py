from pylab import *

input_data = loadtxt("data_prob8.txt");
data=input_data.reshape(1000);


def product(start,end):
	global data;

	prod = 1;
	for i in range(start,end+1):
		prod = prod*data[i];
	return prod;

def zero_checker(start,end):
	flag = -1;
	for i in range(start,end+1):
		if(data[i]==0):
			flag = i;
	print flag;	
	return flag;

max_product = product(0,12);
i = 13;
first = data[0];
flag = 1;

while (i<1000):

	if(data[i]==0):
		i = i+13;
		flag = 0;
	else:
		posn = zero_checker(i-12,i);
		if not (posn==-1):
			i = posn+13;
			flag = 0;
			
	
		else:
			if flag==0:
				temp_prod = product(i-12,i);
				if(temp_prod>max_product):
					max_product = temp_prod;
					flag = 1;
			else:
				if (data[i]>first):
					max_product = product(i-12,i);
					print "HELLO";
					flag = 1;

			first = data[i-12];
			i=i+1;

print max_product;	
