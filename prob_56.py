from pylab import *

def sumofdigits(p):
	t = p;
	s = 0;
	while t>0:
		s = s + t%10;
		t = int(t/10);
	return s;

maximum = 2;
for a in range(2,100):
	for b in range(2,100):
		power = a**b;
		s = sumofdigits(power);
		if s>maximum:
			maximum = s;
print maximum;
