from pylab import *

marked = zeros(28124);
abundant = [];

def sum_finder(number):
	marked = zeros(number);
	sums = 1;
	factor = 2;
	while factor < sqrt(number):	
		if marked[factor]==0:
			if number%factor == 0:
				sums += factor;
				sums += number/factor;
			else:
				i = factor;
				while i < number:
					marked[i]=1;
					i+=factor;
		factor+=1;
	if number%(sqrt(number))==0:
		sums+= int(sqrt(number));
	return sums;
			 	

for number in range(2,28124):
	sums = sum_finder(number);
	if sums > number:
		abundant.append(number);	

for i in range(0,len(abundant)):
	for j in range(i,len(abundant)):
		if abundant[i]+abundant[j]<=28123:
			marked[abundant[i]+abundant[j]]=1;

print sum(where(marked==0)[0]);
