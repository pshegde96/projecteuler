from pylab import *

count = 0;

chain1 = [1];
chain89 = [89];
for i in range(1,568):
	t = i;
	data = [];
	while True:
		if t in chain1:
			chain1.extend(data);
			break;
		elif t in chain89:
			chain89.extend(data);
			break;
		
		data.append(t);
		digits = [];
		n = t;
		while n>0:
			digits.append(n%10);
			n = int(n/10);
		t = sum(array(digits)**2);

for a in range(0,10):
	for b in range(0,a+1):
		for c in range(0,b+1):
			for d in range(0,c+1):
				for e in range(0,d+1):
					for f in range(0,e+1):
						for g in range(0,f+1):
							#100 is just a pseudo digit added in order to help in the algorithm to find repetitive digits.
							num = array([a,b,c,d,e,f,g,100]);
							if sum(num[0:-1]**2)in chain89:
								num.sort();
								d_count=1;
								digit = num[0];
								denom_prod=1;
								for i in range(1,8):
									if num[i]==digit:
										d_count+=1;
									else:
										denom_prod=denom_prod*math.factorial(d_count);
										d_count = 1;
										digit = num[i];
								count = count+(math.factorial(7)/denom_prod);
print count;
							
