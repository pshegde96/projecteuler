from pylab import *

#Form a list of triangle numbers
triangle = [];
sums=0;
for i in range(1,51):
	sums = sums+i;
	triangle.append(sums);

f = open("data_prob42.txt",'r');
count = 0;
for line in f:
	word = line[1:-2];
	word_sum = 0;
	for letter in word:
		word_sum += ord(letter)-64;
	if word_sum in triangle:
		count+=1;
print count;
