from pylab import *

value = 3;
primes = [2];
marked = [0]*1000000;
while value<1000000:
	if marked[value]==0:
		primes.append(value);
		i = value;
		while i < 1000000:
			marked[i]=1;
			i+=value;
	value+=2;

data = [0]*1000000;
data[0] = 1;
i = 0;
while primes[i]<(1000000/max(data)):
	print primes[i];
	k=i;
	sums = 0;
	count = 0;
	while sums<1000000:
		if sums in primes:
			data[sums]= max(data[sums],count);
		sums= sums + primes[k];
		count+=1;
		k = k+1;
	i=i+1;
maximum = max(data);
print where(array(data)==maximum)[0];
