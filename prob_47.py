from pylab import *

i = 2;
flag = 5;

marked = [0]*1000000;
primes = [2];
value = 3;

while value<1000000:
	if marked[value]==0:
		primes.append(value);
		i=value;
		while i<1000000:
			marked[i]=1;
			i+=value;
	value+=2;


def no_distinct_primes(number):
	j=0;
	count = 0;	
	while count<=5 and primes[j]<number:
		if number%primes[j]==0:
			count+=1;
		j+=1;
	#print count;	
	return count;

i = 1;
first = 1;
second = 1;
third = 2;
while flag > 0:
	#print i;
	first = no_distinct_primes(i);
	incr = 1;
	if first == 4:
		second = no_distinct_primes(i+1);
		if second == 4:
			third = no_distinct_primes(i+2);
			if third == 4:
				fourth = no_distinct_primes(i+3);
				if fourth == 4:
					flag =0;
				else:
					incr = 4;
			else:
				incr = 3;
		else:
			incr = 2;
			
	i += incr;

print i-1;
