marked = [0] * 1000000
value = 3
primes = [0]*1000000;
primes[2] = 1;
s = 0;
while value < 1000000:
    if marked[value] == 0:
	primes[value] = 1;
        i = value
        while i < 1000000:
            marked[i] = 1
            i += value
    value += 2

count = 0;
for i in range(11,1000000):
	num = i;
	flag1 = 1;
	#Truncate from right
	while num>0:
		if primes[num]==0:
			flag1 = 0;
			break;
		num = int(num/10);
	if flag1 == 1:
		#Truncate from left
		flag2 = 1;
		num = i;
		while flag2==1:
			if primes[num]==0:
				flag2 = 0;
				break;
			if num<10:
				break;
			num = int(str(num)[1:]);

		if flag2 == 1:
			count += 1;
			s = s + i;
		#	print i;
	if count==11:
		break;

if count==11:
	print s;
else:
	print "Not enough numbers"
