from pylab import *

number = 1;
powers = [1,3,7,15,31,62,125,250,500,1000];#The power set to go through to reach 1000

for i in range(0,10):
	if powers[i]%2==0:
		number = number*number;
	else:
		number = number*number*2;

temp = number;
digits = [];

while temp>0:
	digits.append(temp%10);
	temp = temp/10;

print sum(digits);
