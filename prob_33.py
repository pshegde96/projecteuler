results = [];
def digit_finder(num):
	a = num%10;
	b = (int(num/10))%10;
	return (a,b);

for i in range(11,99):
	for j in range(12,99):
		flag = 0;
		if i!=j and i%10!=0 and j%10!=0:
			(ia,ib) = digit_finder(i);
			(ja,jb) = digit_finder(j);
			if ia==ja:
				i_new = ib;
				j_new = jb;
				flag = 1;
			if ia==jb:
				i_new = ib;
				j_new = ja;
				flag = 1;
			if ib==ja:
				i_new = ia;
				j_new = jb;
				flag = 1;
			if ib==jb:
				i_new = ia;
				j_new = ja;
				flag = 1;
			if flag == 1 and (float(i_new)/float(j_new)==float(i)/float(j)):
				results.append((i,j));
print results;
