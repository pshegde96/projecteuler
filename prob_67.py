#Same solution for problem 67, but data is different

from pylab import *

data = loadtxt("data_prob67.txt");
size = len(data[0,:]);

sums = zeros((size,size)); #Stores the maximum sum from the bottom to that element.

#The maximum sum for the last row is the value of that row itself
sums[size-1,:] = data[size-1,:];

#Go from bottom to the top, max sum till the element from bottom will be data[i,j] + max of the sums of adjacent elements.
i = size-2;
while i>=0:
	for j in range(0,i+1):
		sums[i,j] = data[i,j] + max(sums[i+1,j],sums[i+1,j+1]);
	i=i-1;

print sums[0,0];
	
