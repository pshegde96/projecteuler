from pylab import *

data = loadtxt("data_prob13.txt");
answer = zeros(52,dtype=int);
carry = 0;
for i in range(0,50):
	summ = carry + sum(data[:,49-i]);
	answer[i] = summ%10;
	carry = summ/10;
answer[50] = carry%10;
answer[51] = carry/10%10;
print answer[40:];
