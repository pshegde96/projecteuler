from pylab import *

#First find prime numbers upto a safe limit, so as to compare with the values of the equations easily.
marked = [0] * 2000000
value = 3
s = [2]
while value < 2000000:
    if marked[value] == 0:
        s.append(value);
        i = value
        while i < 2000000:
            marked[i] = 1
            i += value
    value += 2
#Prime number array upto the value of 2000000
primes = array(s);

# b has to be positive prime, as for n=0,y = b.
a = -999;
b = 2;
maximum = 0;
amax = -999;
bmax = 2;
while a < 1000:
	counter = where(primes>maximum)[0][0];
	b = primes[counter];
	while b < 1000:
		flag = -1;
		n = 0;
		temp = maximum*maximum + a *maximum + b;
		if not((size(where(primes==temp)[0])==0)):
			while flag == -1:
				y = n*n + a*n + b;
				if size(where(primes==y)[0])==0:
					flag = 1;
					if n > maximum:
						maximum = n;
						amax = a;
						bmax = b;
				n += 1;
		counter = counter + 1;
		b = primes[counter];
	a = a + 2;

print amax*bmax;
