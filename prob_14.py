from pylab import *

data_set = zeros(1000001);
number = 1;
flag = -1;

while number<=1000000:
	temp = number;
	count = 0;
	while flag==-1:
		if temp<=1000000:
			if data_set[temp]==0:
				if temp==1:
					count+=1;
					flag = 1; 	
				else:
					if temp%2==0:
						temp = temp/2;
						count+=1;
					else:
						temp = 3*temp + 1;
						count+=1;
			else:
				count+=data_set[temp];
				flag = 1;
		else:
			if temp%2==0:
				count+=1;
				temp = temp/2;
			else:
				count+=1;
				temp = 3*temp+1;
	data_set[number]=count;
	number+=1;
	flag=-1;
maximum = max(data_set);
print where(data_set==maximum)[0][0]; 
